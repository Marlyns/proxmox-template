### Ubuntu or Debian template with Packer

Create an Ubuntu or Debian VM template using Packer for Proxmox.
For more details and a video of Packer in action see my blog [post](https://itnoobs.net/automation/proxmox-automation). 

### Prerequisites

- Internal DHCP and access to internet (if installing extra packages)
- Proxmox VE 6.x host (tested on PVE 6.1-8)
- Linux or Windows control host
- Packer binaries - i.e. [Download] binary and copy to `/usr/bin` on Linux or `c:\program files\packer` on Windows
- Ubuntu or Debian ISO on the Proxmox host


### Getting Started

- Clone or download this repository 
  - `git clone git@gitlab.com:itnoobs-automation/packer/proxmox-template.git`
  - `wget https://gitlab.com/itnoobs-automation/packer/proxmox-template.git`
- Save Proxmox password in an environment variable - i.e. on Linux `export PASS="password"` or `$PASS = "password"` on Windows PowerShell
- Generate a new password hash for preseed - i.e. `openssl passwd -6 -salt <saltvalue> <password>` and replace the current value. If you prefer to use the current value, the password will be set to: `P@ssw0rd12`
- Change directory, `cd proxmox-template`
- Validate configuration 
  - `packer validate ubuntu-18.04.json` 
  - `packer validate debian-10.json`
- Create VM template 
  - `packer build ubuntu-18.04.json` 
  - `packer build debian-10.json`